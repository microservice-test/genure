const express = require("express");
const { check, validationResult } = require("express-validator");
const Genures = require("./models/genure_model");
var mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());

const validatorPost = [
  check("genure_name").not().isEmpty().trim().escape(),
  (req, res, next) => {
    const errors = validationResult(req).array();
    if (errors.length) {
      return res.status(422).json({ errors });
    }
    next();
  },
];

//API to get the list of genures
app.get("/api/v1/genures", async (req, res) => {
  const genures = await Genures.find({});
  res.json(genures);
});

//API to get the details of specific genure
app.get("/api/v1/genures/:id", async (req, res) => {
  const genures = await Genures.findById(req.params.id);
  res.json(genures);
});

//API to save genure data
app.post("/api/v1/genures", validatorPost, async (req, res) => {
  const genure = new Genures({
    genure_name: req.body.genure_name,
    description: req.body.description,
    status: true,
  });
  const savedGenure = await genure.save();
  res.json(savedGenure);
});

//API to update genure data
app.put("/api/v1/genures/:id", validatorPost, async (req, res) => {
  const savedGenure = await Genures.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        genure_name: req.body.genure_name,
        description: req.body.description,
        status: req.body.status,
      },
    }
  );
  res.json(savedGenure);
});

//API to delete genure data
app.delete("/api/v1/genures/:id", async (req, res) => {
  //delete genure from the movies
  //let deleteMovieGenure = axios.
  //then delete the genure from the table.
  const genures = await Genures.findOneAndDelete({ _id: req.params.id });
  res.json(genures);
});

module.exports = app;
