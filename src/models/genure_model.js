const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GenureSchema = new Schema(
  {
    genure_name: {
      type: String,
      required: true,
    },
    description: { type: String, required: false },
    status: { type: Boolean, required: true, default: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Genure", GenureSchema);
