const mongoose = require("mongoose");
const GenureModel = require("../src/models/genure_model");
const GenureData = {
  genure_name: "Genure Test",
  description: "Genure Description",
};
const GenureDataUpdate = {
  genure_name: "Genure Test Update",
  description: "Genure Description Update",
};
const request = require("supertest");
const app = require("../src/app");

describe("Genure API TEST", () => {
  let idSaved = "";
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll((done) => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
    done();
  });

  it("should check post request with required fields", async () => {
    const res = await request(app).post("/api/v1/genures").send(GenureData);
    idSaved = res.body._id;
    //console.log(res.body);
    expect(res.status).toBe(200);
    expect(res.body.genure_name).toBe(GenureData.genure_name);
    expect(res.body.description).toBe(GenureData.description);
    expect(res.body.status).toBe(true);
  });

  it("should check get request", async () => {
    const res = await request(app).get("/api/v1/genures");
    expect(res.status).toBe(200);
  });

  it("should check get request for specific record", async () => {
    //console.log(idSaved);
    const res = await request(app).get("/api/v1/genures/" + idSaved);
    expect(res.status).toBe(200);
    expect(res.body._id).toBe(idSaved);
  });

  it("should check put request for specific record", async () => {
    const res = await request(app)
      .put("/api/v1/genures/" + idSaved)
      .send(GenureDataUpdate);
    expect(res.status).toBe(200);
    expect(res.body._id).toBe(idSaved);
  });

  it("should check delete request", async () => {
    const res = await request(app).delete("/api/v1/genures/" + idSaved);
    expect(res.status).toBe(200);
  });
});
