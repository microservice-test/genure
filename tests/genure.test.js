const mongoose = require("mongoose");
const GenureModel = require("../src/models/genure_model");
const GenureData = {
  genure_name: "Genure Test",
  description: "Genure Description",
};

describe("Genure Schema Test", () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll((done) => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
    done();
  });

  it("create & save Genure successfully", async () => {
    const validGenure = new GenureModel(GenureData);
    const savedGenure = await validGenure.save();
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedGenure._id).toBeDefined();
    expect(savedGenure.genure_name).toBe(GenureData.genure_name);
    expect(savedGenure.description).toBe(GenureData.description);
  });

  // You shouldn't be able to add in any field that isn't defined in the schema
  it("insert Genure successfully, but the field does not defined in schema should be undefined", async () => {
    const GenureWithInvalidField = new GenureModel({
      genure_name: "Genure Test",
    });
    const savedGenureWithInvalidField = await GenureWithInvalidField.save();
    expect(savedGenureWithInvalidField._id).toBeDefined();
    expect(savedGenureWithInvalidField.description).toBeUndefined();
  });

  // It should give error on the genure_name field
  it("create Genure without required field should failed", async () => {
    const GenureWithoutRequiredField = new GenureModel({});
    let err;
    try {
      const savedGenureWithoutRequiredField = await GenureWithoutRequiredField.save();
      error = savedGenureWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.genure_name).toBeDefined();
  });

  // It should us told us the errors in on gender field.
  it("get method should give data", async () => {
    const savedGenureWithoutRequiredField = await GenureModel.findOne();
    expect(savedGenureWithoutRequiredField._id).toBeDefined();
    expect(savedGenureWithoutRequiredField.genure_name).toBeDefined();
  });
});
